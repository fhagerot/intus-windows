<?php


namespace Shortener\Domain\Contracts\Repositories;


use Shortener\Domain\Entities\Url;

interface UrlsRepository
{

    public function getByHash(string $hash): ?Url;

    public function getByValue(string $value): ?Url;

    public function persist(Url $url): void;
}
