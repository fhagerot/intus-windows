<?php

namespace Shortener\Domain\Contracts\Commands;


interface ShortUrl
{

    public function getUrl(): string;
}
