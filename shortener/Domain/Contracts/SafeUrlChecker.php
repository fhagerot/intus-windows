<?php


namespace Shortener\Domain\Contracts;


interface SafeUrlChecker
{

    public function isSafe(string $url): bool;
}
