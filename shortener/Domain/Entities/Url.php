<?php

declare(strict_types=1);

namespace Shortener\Domain\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string value
 * @property string hash
 * @property int user_id
 */
class Url extends Model
{

    protected $fillable = [
        'value',
        'hash',
    ];
}
