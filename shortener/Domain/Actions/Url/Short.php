<?php

declare(strict_types=1);

namespace Shortener\Domain\Actions\Url;


use Shortener\Domain\Contracts\Commands\ShortUrl;
use Shortener\Domain\Contracts\Repositories\UrlsRepository;
use Shortener\Domain\Contracts\SafeUrlChecker;
use Shortener\Domain\Entities\Url;

class Short
{

    private SafeUrlChecker $urlChecker;

    private UrlsRepository $urls;

    public function __construct(SafeUrlChecker $urlChecker, UrlsRepository $urls)
    {
        $this->urlChecker = $urlChecker;
        $this->urls = $urls;
    }

    public function call(ShortUrl $command): string
    {
        if (!$this->urlChecker->isSafe($command->getUrl())) {
            throw new \Exception('Url is not safe');
        }

        if ($url = $this->urls->getByValue($command->getUrl())) {
            return $url->hash;
        }

        $hash = $this->getSixSymbolsHash();

        $url = new Url([
            'value' => $command->getUrl(),
            'hash'  => $hash,
        ]);

        $this->urls->persist($url);

        return $hash;
    }

    private function getSixSymbolsHash(): string
    {
        return substr(md5(uniqid((string)rand(), true)), 0, 6);
    }
}
