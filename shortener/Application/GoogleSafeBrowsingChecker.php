<?php

declare(strict_types=1);

namespace Shortener\Application;


use Shortener\Domain\Contracts\SafeUrlChecker;
use Snipe\Safebrowsing\Safebrowsing;

class GoogleSafeBrowsingChecker implements SafeUrlChecker
{

    public function isSafe(string $url): bool
    {
        $safebrowsing = new Safebrowsing;

        $safebrowsing->addCheckUrls([$url]);
        $safebrowsing->execute();

        return !$safebrowsing->isFlagged($url);
    }
}
