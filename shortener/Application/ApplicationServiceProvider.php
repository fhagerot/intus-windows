<?php

declare(strict_types=1);

namespace Shortener\Application;


use Illuminate\Support\ServiceProvider;
use Shortener\Domain\Contracts\SafeUrlChecker;

class ApplicationServiceProvider extends ServiceProvider
{

    public function register(): void
    {
        $this->app->bind(SafeUrlChecker::class, GoogleSafeBrowsingChecker::class);
    }
}
