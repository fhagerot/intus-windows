<?php

declare(strict_types=1);

namespace Shortener\Infrastructure;


use Illuminate\Support\ServiceProvider;
use Shortener\Infrastructure\Repositories\RepositoriesServiceProvider;

class InfrastructureServiceProvider extends ServiceProvider
{

    public function boot(): void
    {
        $this->app->register(RepositoriesServiceProvider::class);
    }
}
