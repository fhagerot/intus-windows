<?php

declare(strict_types=1);

namespace Shortener\Infrastructure\Repositories;


use Shortener\Domain\Contracts\Repositories\UrlsRepository as UrlsRepositoryContract;
use Shortener\Domain\Entities\Url;

class UrlsRepository implements UrlsRepositoryContract
{

    public function getByHash(string $hash): ?Url
    {
        /** @var Url|null $url */
        $url = Url::query()
            ->where('hash', $hash)
            ->first();

        return $url;
    }

    public function getByValue(string $value): ?Url
    {
        /** @var Url|null $url */
        $url = Url::query()
            ->where('value', $value)
            ->first();

        return $url;
    }

    public function persist(Url $url): void
    {
        $url->save();
    }
}
