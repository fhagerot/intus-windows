<?php

declare(strict_types=1);

namespace Shortener\Infrastructure\Repositories;


use Illuminate\Support\ServiceProvider;
use Shortener\Domain\Contracts\Repositories as RepositoryContracts;

class RepositoriesServiceProvider extends ServiceProvider
{

    private array $repositoryImplementations = [
        RepositoryContracts\UrlsRepository::class => UrlsRepository::class,
    ];

    public function register(): void
    {
        $this->registerRepositoryImplementations();
    }

    private function registerRepositoryImplementations(): void
    {
        foreach ($this->repositoryImplementations as $contract => $implementation) {
            $this->app->bind($contract, $implementation);
        }
    }
}
