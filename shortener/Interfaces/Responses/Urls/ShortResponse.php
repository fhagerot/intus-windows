<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Responses\Urls;


use Illuminate\Contracts\Support\Responsable;

class ShortResponse implements Responsable
{

    private string $shortUrl;

    public function __construct(string $shortUrl)
    {
        $this->shortUrl = $shortUrl;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'short_url' => $this->shortUrl,
        ]);
    }
}
