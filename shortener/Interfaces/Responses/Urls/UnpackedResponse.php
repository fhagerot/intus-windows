<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Responses\Urls;


use Illuminate\Contracts\Support\Responsable;

class UnpackedResponse implements Responsable
{

    private string $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'url' => $this->url,
        ]);
    }
}
