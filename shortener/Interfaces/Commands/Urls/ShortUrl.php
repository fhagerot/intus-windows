<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Commands\Urls;


use Shortener\Domain\Contracts\Commands\ShortUrl as ShortUrlCommand;
use Shortener\Infrastructure\BaseCommand;

class ShortUrl extends BaseCommand implements ShortUrlCommand
{

    private ?string $url = null;

    public function rules(): array
    {
        return [
            'url' => [
                'required',
                'url',
            ],
        ];
    }

    protected function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }
}
