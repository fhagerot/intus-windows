<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Http\Api;


use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Route;

class HttpServiceProvider extends RouteServiceProvider
{

    public function boot(): void
    {
        $this->bootRoutes();
    }

    private function bootRoutes(): void
    {
        $this->routes(
            function () {
                Route::prefix('api')
                    ->middleware('api')
                    ->group(base_path('Shortener/Interfaces/Http/Api/routes.php'));
            });
    }
}
