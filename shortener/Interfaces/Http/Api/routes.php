<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Shortener\Interfaces\Http\Api\Actions\Urls;

Route::post('/urls', Urls\ShortAction::class);
Route::get('/urls/{hash}', Urls\UnpackAction::class);
