<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Http\Api\Actions\Urls;

use Shortener\Domain\Actions\Url;
use Shortener\Interfaces\Commands\Urls\ShortUrl;
use Shortener\Interfaces\Responses\Urls\ShortResponse;

class ShortAction
{

    public function __invoke(ShortUrl $command, Url\Short $shortUrl)
    {
        return new ShortResponse(
            url('/' . $shortUrl->call($command))
        );
    }
}
