<?php

declare(strict_types=1);

namespace Shortener\Interfaces\Http\Api\Actions\Urls;


use Shortener\Infrastructure\Repositories\UrlsRepository;
use Shortener\Interfaces\Responses\Urls\UnpackedResponse;

class UnpackAction
{

    public function __invoke(string $hash, UrlsRepository $urls)
    {
        $url = $urls->getByHash($hash);

        if (empty($url)) {
            throw new \Exception('Not found entity');
        }

        return new UnpackedResponse(
            $url->value
        );
    }
}
