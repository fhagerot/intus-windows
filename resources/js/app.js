require('./bootstrap');
import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import VueAxios from 'vue-axios';
import router from './router'
import { BootstrapVue } from 'bootstrap-vue'

Vue.use(BootstrapVue)
Vue.use(VueAxios, axios);

axios.defaults.baseURL = `http://intus-windows.loc/api`;

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')
